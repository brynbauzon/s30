//Step 7 import express
const express = require('express');

const mongoose = require('mongoose');

//S8 Create express app
const app = express();

//S9Create a const called port
const port = 3001;

//S10 Allow our app to read JSON JSON data
app.use(express.json());

//S11 Allow our app to read data from forms
app.use(express.urlencoded({extended:true}));



//S12 Install Mongoode npm install mongoose
//Mongoose is a package that allows creation of Schemas to model our data structures

//S13 const mongoose = require('mongoose');

//S1 4Go to MongoDB Atlast and change the Network access to 0.0.0.0

//S15 Get connection string
//mongodb+srv://admin:admin123@zuitt-bootcamp.fs2pp.mongodb.net/myFirstDatabase?retryWrites=true&w=majority


//S16 Change myFirstData to s30 Mongo DB will automatically create the database for us.
//mongodb+srv://admin:admin123@zuitt-bootcamp.fs2pp.mongodb.net/s30?retryWrites=true&w=majority

//Step 17 Connecting to MongoDB atlast - add.connect() mehtod
//mongoose.connect()

//Step 18 add the connection string 
//Step 18B add the this object to allow connection
/*
	{
		useNewUrlParser: true
		useUnifiedTopology: true

	}


*/
mongoose.connect('mongodb+srv://admin:admin123@zuitt-bootcamp.fs2pp.mongodb.net/s30?retryWrites=true&w=majority',

	{
		useNewUrlParser: true,
		useUnifiedTopology: true

	}	

);

//Step 19 Set notification for connection success or failure by using .connection property of mongoose
//mongoose.connection;

//Step 20 Store it in a variable called db.
let db = mongoose.connection; 

//Step 21 console.error.bind(console) allows us to 
//print errors in the browser console and in the terminal
db.on('error', console.error.bind(console, 'conection error'));

//Step 22 if the connection is successful, out in this in the console
db.once('open',()=>{

	console.log('Were connected to the cloud database');
});


//Step 23 Schemas determine the structure of the documents to be written in the database
//Schemas acts as blueprints to our data
//Use the Schema constructor of the Mongoose module to create a new Schema object
const taskSchema = new mongoose.Schema({
  //Define the fields with corresponding data type
  //There is a field called "name" and its data type is "String"
  name: String,
  status: {    
  //There is a called "status" that is "String" and the default value is "pending"
  		type: String,
  		default: "pending"
  }

});

//Models use Schemas and they act as the middleman from the Server (JS Code) to our database
//Step 24 Create a model
//Model must be in singular form and capitalized
//The first paramater of the Mongoose model method indicates the collection
//in where to store the data (tasks, plural form of the model)

const Task = mongoose.model('Task', taskSchema);

/*
Business Logic
1. Add a functionality to check if there are duplicate tasks
	-If the task already exists in the database, we return an error,
	-If the task doesn't exist in the datbase, we add it in the database 

*/


//Step 25 Create route to add task
// app.post('/',(req, res)=>{

// });

//Step 26 Check if the task is already exist
//Use the Task model to interact with the tasks collectio




//HTML Form - POSTman
//req.body
//req.body.name

/*
Submit Registration
lname:
fname:
email:
*/


app.post('/tasks', (req, res)=>{

Task.findOne({
	name: req.body.name
	},(err, result)=>{
   		
   		if(result != null && result.name == req.body.name){
   			//Return a message to the client/Postman
   			return res.send('Duplicate task found')
        console.log(result);
   		}else{

   			let newTask = new Task({
   				name: req.body.name
   				});

	   				newTask.save((saveErr, saveTask)=>{
	   					if(saveErr){

	   						return console.error(saveErr);
	   					}else{

	   						return res.status(201).send('New task created');
	   					}

	   				});
   			


   		}   
   
	});

});

//the result of findOne is "name": "code" == req.body.name(code)


//Step 27 Get all tasks

app.get('/tasks',(req, res)=>{
  Task.find({}, (err, result)=>{

  	if(err){
  		 return console.log(err);

  	}else{

  		return res.status(200).json({data: result})
  	}


  });

	
});


//Create a User Scheme
/* 

 username: String,
 password: String

*/

const userSchema = new mongoose.Schema({


username: String,
password: String


});


//Make a Model
const User = mongoose.model("User", userSchema)


//Register a User


app.post('/signup', (req, res)=>{

User.findOne({
	username: req.body.username
	},(err, result)=>{
   		
   		if(result != null && result.username == req.body.username){
   			
   			return res.send('Duplicate user found')

   		}else{

   			let newUser = new User({
   				username: req.body.username,
   				paswword: req.body.password
   				});

	   				newUser.save((saveErr, saveUser)=>{
	   					if(saveErr){

	   						return console.error(saveErr);
	   					}else{

	   						return res.status(201).send('New users created');
	   					}

	   				});
   			


   		}   
   
	});

});


app.listen(port, ()=>console.log(`Server running at port ${port}`));